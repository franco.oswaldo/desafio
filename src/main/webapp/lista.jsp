<%@page import="java.util.Properties"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.net.URL"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONException"%>
<%@page import="org.json.JSONObject" %>
<%@page import="org.json.JSONString"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lista de personajes</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css">
<link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
<link href="assets/css/style.css">


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
<script src="assets/js/prueba.js"></script>

    <%
		URL jsonpage = new URL("https://api.got.show/api/book/characters");
		HttpURLConnection urlcon = (HttpURLConnection) jsonpage.openConnection();
		urlcon.setRequestMethod("GET");
		urlcon.setRequestProperty("Accept", "application/json");
		BufferedReader br = new BufferedReader(new InputStreamReader((urlcon.getInputStream())));
		String output = "";
		output = br.readLine();
		JSONArray json_arr = new JSONArray(output);
		int cant = json_arr.length();
       	urlcon.disconnect();
    %>

</head>
 <body class="d-flex flex-column h-100">
	<header>
	  <!-- Fixed navbar -->
	  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
	    <div class="container-fluid">
	      <a class="navbar-brand" href="#">Navbar</a>
	      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	      </button>
	      <div class="collapse navbar-collapse" id="navbarCollapse">
	        <ul class="navbar-nav me-auto mb-2 mb-md-0">
	          <li class="nav-item">
	            <a class="nav-link" aria-current="page" href="index.jsp">Home</a>
	          </li>
	          <li class="nav-item">
	            <a class="nav-link active" aria-current="page" href="#">Listar</a>
	          </li>
	        </ul>
	      </div>
	    </div>
	  </nav>
	</header>
	
	<!-- Begin page content -->
	<main class="flex-shrink-0">
	  <div class="container px-4 py-5">
	    <h1 class="mt-5">Listado de personajes</h1>
	    <table id="example" class="table table-striped" style="width:100%">
	        <thead>
	            <tr>
	                <th>Name</th>
	                <th>House</th>
	                <th>Accion</th>
	            </tr>
	        </thead>
	        <tbody>
	        	<%
		        	for(int i=0;i<cant;i++){	
		        		String House = "";
						String Name = json_arr.getJSONObject(i).getString("name");
						try{
							House = json_arr.getJSONObject(i).getString("house");
						}catch(Exception e){
							House = "";
						}
						
						String Slug = json_arr.getJSONObject(i).getString("slug");
						JSONObject obj = json_arr.getJSONObject(i);
						
						session.setAttribute("objetos", obj);
				%>
						<tr>
							<td><%= Name %></td>
							<td><%= House %></td>
							<td><a class="btn btn-primary" href="view.jsp?slug=<%= Slug %>" role="button">Ver</a></td>
						</tr>
				<%	
					} 
	        	%>
	        </tbody>
        </table>
	  </div>
	</main>
</body>
</html>