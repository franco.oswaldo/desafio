

<%@page import="java.util.Properties"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.net.URL"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONException"%>
<%@page import="org.json.JSONObject" %>
<%@page import="org.json.JSONString"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>View personaje</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css">
<link href="assets/css/style.css">


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"></script>

</head>
 <body class="d-flex flex-column h-100">
	<header>
	  <!-- Fixed navbar -->
	  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
	    <div class="container-fluid">
	      <a class="navbar-brand" href="#">Navbar</a>
	      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	      </button>
	      <div class="collapse navbar-collapse" id="navbarCollapse">
	        <ul class="navbar-nav me-auto mb-2 mb-md-0">
	          <li class="nav-item">
	            <a class="nav-link" aria-current="page" href="index.jsp">Home</a>
	          </li>
	          <li class="nav-item">
	            <a class="nav-link" aria-current="page" href="lista.jsp">Listar</a>
	          </li>
	          <li class="nav-item">
	            <a class="nav-link disabled" tabindex="-1" aria-disabled="true">View</a>
	          </li>
	        </ul>
	       
	      </div>
	    </div>
	  </nav>
	</header>
	<%
		
		
		String slug = "";
		String name = "";
		String image = "";
		String gender = "";
		String house = "";
		String Titles;
		int rank = 0;
		JSONArray titles = null;
		try{
   			slug = request.getParameter("slug");
   			URL jsonpage = new URL("https://api.got.show/api/book/characters/bySlug/"+slug);
   	   		HttpURLConnection urlcon = (HttpURLConnection) jsonpage.openConnection();
   	   		urlcon.setRequestMethod("GET");
   	   		urlcon.setRequestProperty("Accept", "application/json");
   	   		BufferedReader br = new BufferedReader(new InputStreamReader((urlcon.getInputStream())));
   	   		String output = "";
   	   		output = br.readLine();
   	   		JSONObject obj = new JSONObject(output);
   	   		JSONObject pagerank = obj.getJSONObject("pagerank");
   	   		titles = obj.getJSONArray("titles");
   	   		name = obj.getString("name");
   	   		
   	   		try{
   	   			image = obj.getString("image");
   	   		}catch(Exception e){
   	   			image = "https://pic.onlinewebfonts.com/svg/img_546302.png";
   	   		}
   	   		
   	   		try{
   	   			gender = obj.getString("gender");
   	   		}catch(Exception e){
   	   			gender = "--";
   	   		}
   	   		
   	   		
   	   		rank = pagerank.getInt("rank");
   	   		
   	   		
   	   		try{
   	   			house = obj.getString("house");
   	   		}catch(Exception e){
   	   			house = "--";
   	   		}
   	   		
   	   		
   	   		try{
   	   			Titles = obj.getString("titles");
   	   		}catch(Exception e){
   	   			Titles = "--";
   	   		}
		}catch(Exception e){
			out.println ("El error es: " + e.getMessage());
		    e.printStackTrace();
		}
		if (slug == null || slug.trim().isEmpty()) {
			response.sendRedirect("lista.jsp");
		}
	
       	
    %>
	<!-- Begin page content -->
	<main class="flex-shrink-0">
	  <div class="container px-4 py-5">
	    <h1 class="mt-5">Información del personaje <b>"<%=name %>"</b></h1>
	    <table id="example" class="table table-striped" style="width:100%">
	        <tbody>
	        	<tr>
	        		<td>
	        			<b>Imagen</b>
	        		</td>
	        		<td>
       					<div class="text-center">
						  <img src="<%=image %>" class="rounded" alt="..." width='10%' height='10%'>
						</div>
	        		</td>
	        	</tr>
	        	<tr>
	        		<td>
	        			<b>Sexo</b>
	        		</td>
	        		<td>
	        			<%= gender %>
	        		</td>
	        	</tr>
	        	<tr>
	        		<td>
	        			<b>Slug</b>
	        		</td>
	        		<td>
	        			<%= slug %>
	        		</td>
	        	</tr>
	        	<tr>
	        		<td>
	        			<b>Rank</b>
	        		</td>
	        		<td>
	        			<%= rank %>
	        		</td>
	        	</tr>
	        	<tr>
	        		<td>
	        			<b>Casa</b>
	        		</td>
	        		<td>
	        			<%= house %>
	        		</td>
	        	</tr>
	        	<tr>
	        		<td>
	        			<b>Títulos</b>
	        		</td>
	        		<td>
	        			<%
	        				if(titles != null && titles.length() > 0 ){
		        				for( int y=0;y < titles.length(); y++){
		        					out.println(titles.getString(y)+"<br>");
		        				}
	        				}else{
	        					out.println("--");
	        				}
	        			%>
	        		</td>
	        	</tr>
	        </tbody>
        </table>
	  </div>
	</main>
</body>
</html>