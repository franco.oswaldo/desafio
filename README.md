# Desafio



## Para iniciar

Clone el repositorio y vaya a la carpeta del proyecto
```
git clone https://gitlab.com/franco.oswaldo/frontend.git
cd frontend/
```

- Abra el proyecto con Eclipse, el cual ya debe tener configurado el servidor Tomcat.


- Luego ejecute el servidor Tomcat


- Una vez iniciado el Tomcat, visite [http://localhost:PUERTO_TOMCAT/frontend](http://localhost:PUERTO_TOMCAT/frontend)


- Path frontend = /src/main/webapp/

- Path Backend = /src/main/got/
